import { defineConfig } from 'umi';

const isDev = process.env.NODE_ENV === 'development';

export default defineConfig({
  antd: {},
  locale: { antd: true },
  publicPath: isDev ? '/' : './',
  history: {
    type: 'hash',
  },

  routes: [
    {
      path: '/',
      redirect: '/home',
    },

    { path: '/home', component: '@/pages/Home/index' },
    { path: '/diff', component: '@/pages/Diff/index' },
    { path: '/diff/from-upload', component: '@/pages/Diff/FromUpload' },
  ],

  fastRefresh: true,
  tailwindcss: {},
  model: {},
  extraBabelIncludes: ['json-diff'],
  mfsu: { exclude: ['json-diff'] },
});
