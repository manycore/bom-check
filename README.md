# 拆单校验工具

## 用途
在拆单类小程序（如解单助手）更新版本后，对其核心的拆单逻辑进行回归测试。

## 开始

安装依赖

```bash
$ yarn
```

启动开发服务器

```bash
$ yarn start
```

## 参考资料
1. [介绍 | 商家后台小程序平台](https://manual.kujiale.com/miniapp-sdk/1.13.0/guides/)