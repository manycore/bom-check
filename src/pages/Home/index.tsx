import { bomApi } from '@/api';
import { addMarkedDesign } from '@/api/store/marked-design';
import { PageContainer } from '@ant-design/pro-layout';
import { useModel } from '@umijs/max';
import { useBoolean, useRequest } from 'ahooks';
import { Modal, Space } from 'antd';
import { history } from 'umi';
import CheckData from './components/CheckData';
import type { IUploadData } from './components/PlankDataUpload';
import { PlanksUpload } from './components/PlankDataUpload';
import SelectDesign from './components/SelectDesign';
import DataConfig from './components/Config';
import {
  defaultDataConfig,
  getDataConfig,
  updateDataConfig,
} from '@/api/data-config';
import { ComponentProps } from 'react';

const log = console.log.bind(null, '[page: Home]');

function toDiffPage() {
  history.push({ pathname: '/diff' });
}
function toDiffFromUploadPage() {
  history.push({ pathname: '/diff/from-upload' });
}
export default function IndexPage() {
  const {
    design: { designId, oldPlanks },
    setDesign,
  } = useModel('bom');
  const planks = oldPlanks;

  const [
    baseCheckModalOpen,
    { setTrue: showBaseCheckModal, setFalse: closeBaseCheckModal },
  ] = useBoolean(false);
  const [
    newCheckModalOpen,
    { setTrue: showNewCheckModal, setFalse: closeNewCheckModal },
  ] = useBoolean(false);

  function handleBaseCheckSuccess() {
    console.log('CheckBaseSuccess');
    Modal.confirm({
      title: '确认提交基准数据!',
      onOk: async () => {
        await bomApi.updateMarkedPlankList({ designId, planks });
        await addMarkedDesign({ designId });
        closeBaseCheckModal();
        showNewCheckModal();
      },
    });
  }
  function handleNewCheckSuccess() {
    console.log('CheckNewSuccess');
    Modal.confirm({
      title: '确认提交对比数据!',
      onOk: () => {
        closeNewCheckModal();
        toDiffPage();
      },
    });
  }
  function handlePlanksUpload(data: IUploadData) {
    log('upload', data);
    setDesign(data);
    toDiffFromUploadPage();
  }

  const {
    data: dataConfig = defaultDataConfig,
    refreshAsync: refreshDataConfig,
  } = useRequest(getDataConfig);
  async function handleUpdateDataConfig(
    config: ComponentProps<typeof DataConfig>['dataConfig'],
  ) {
    await updateDataConfig(config);
    await refreshDataConfig();
  }

  return (
    <PageContainer>
      <Space>
        <SelectDesign
          title="导入方案"
          onConfirm={async ({ designId }) => {
            const planks = await bomApi.getDesignPlanks({ designId });

            setDesign({
              designId,
              oldPlanks: planks,
            });

            const markedPlanks = await bomApi.getMarkedPlankList({
              designId,
            });
            log({ planks, markedPlanks });
            if (markedPlanks.length === 0) {
              showBaseCheckModal();
            } else {
              showNewCheckModal();
            }
          }}
        />
        <PlanksUpload onConfirm={handlePlanksUpload} />
        <DataConfig
          dataConfig={dataConfig}
          onUpdateConfig={handleUpdateDataConfig}
        />
      </Space>
      <CheckData
        title="基准数据确认 | 方案名称(方案id)"
        open={baseCheckModalOpen}
        onOk={handleBaseCheckSuccess}
        onCancel={closeBaseCheckModal}
        okText="确认基准数据"
        dataSource={planks}
      />
      <CheckData
        title="对比数据确认 | 方案名称(方案id)"
        open={newCheckModalOpen}
        onOk={handleNewCheckSuccess}
        onCancel={closeNewCheckModal}
        okText="确认对比数据"
        dataSource={planks}
      />
    </PageContainer>
  );
}
