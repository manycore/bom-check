import { Modal } from '@/components/Modal';
import { columns } from '@/constants/bom';
import { filterCol } from '@/utils/table';
import { ProCard } from '@ant-design/pro-components';
import { List, ModalProps, Table, TableProps } from 'antd';
import { map, unionBy } from 'lodash-es';
// @ts-ignore
import { repeated } from 'futil';
import { FC } from 'react';
import '@/styles/global.less';
import classnames from 'classnames';

interface IProps
  extends Pick<ModalProps, 'title' | 'open' | 'okText' | 'onCancel' | 'onOk'>,
    Pick<TableProps<any>, 'dataSource'> {}

const CheckData: FC<IProps> = (props) => {
  const { title, open, onOk, onCancel, okText, dataSource } = props;
  const modelList = dataSource;
  const modelIdList = map(modelList, 'modelId');
  const uniqOrder = unionBy(modelList, 'orderId');
  const repeatedModelIdList: string[] = repeated(modelIdList);
  const disableConfirm = repeatedModelIdList.length > 0;

  return (
    <Modal
      okButtonProps={{ disabled: disableConfirm }}
      cancelButtonProps={{ style: { display: 'none' } }}
      open={open}
      onOk={onOk}
      onCancel={onCancel}
      title={title}
      fullscreen={{ default: true }}
      okText={okText}
    >
      <ProCard split="vertical" className="h-full">
        <ProCard colSpan="20%" className="h-full overflow-y-auto">
          <List
            itemLayout="horizontal"
            dataSource={map(uniqOrder)}
            renderItem={(order) => (
              <List.Item>
                <List.Item.Meta
                  title={
                    <span
                      className={classnames({
                        'bg-danger': repeatedModelIdList.includes(
                          order.modelId,
                        ),
                      })}
                    >
                      {order.orderName}
                    </span>
                  }
                />
              </List.Item>
            )}
          />
        </ProCard>
        <ProCard className="h-full overflow-y-auto">
          <Table
            columns={filterCol(columns, {
              dataIndexs: ['modelId', 'name', 'plankId'],
            })}
            dataSource={dataSource}
            rowClassName={(model) => {
              const { modelId } = model;
              const isRepeated = repeatedModelIdList.includes(modelId);
              return isRepeated ? 'bg-danger' : '';
            }}
          />
        </ProCard>
      </ProCard>
    </Modal>
  );
};

export default CheckData;
