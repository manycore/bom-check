import { createLog } from '@/utils/console';
import { useBoolean } from 'ahooks';
import { Button, Form, Modal, Select } from 'antd';
import { FC } from 'react';

const log = createLog('[page-comp: DataConfig]');
const title = '数据配置';

interface IDataConfig {
  categorys: string[];
  excludeModelNames: string[];
}

interface IProps {
  dataConfig: IDataConfig;
  onUpdateConfig: (config: IDataConfig) => Promise<void>;
}

const DataConfig: FC<IProps> = (props) => {
  const { dataConfig } = props;
  const { categorys, excludeModelNames } = dataConfig;
  const categoryOptions = dataConfig.categorys.map((key) => ({ value: key }));
  const excludeModelNameOptions = dataConfig.excludeModelNames.map((key) => ({
    value: key,
  }));

  const [dataConfigForm] = Form.useForm<IDataConfig>();
  const [
    dataConfigOpen,
    { setTrue: showDataConfig, setFalse: hideDataConfig },
  ] = useBoolean();

  async function handleOk() {
    const dataConfig = dataConfigForm.getFieldsValue();
    log({ dataConfig });
    await props.onUpdateConfig(dataConfig);
    hideDataConfig();
  }
  function handleCancel() {
    dataConfigForm.resetFields();
    hideDataConfig();
  }

  return (
    <>
      <Button type="primary" onClick={showDataConfig}>
        {title}
      </Button>
      <Modal
        title={title}
        open={dataConfigOpen}
        onCancel={handleCancel}
        onOk={handleOk}
      >
        <Form form={dataConfigForm}>
          <Form.Item initialValue={categorys} name="categorys" label="类别">
            <Select
              mode="tags"
              tokenSeparators={[',']}
              options={categoryOptions}
            />
          </Form.Item>
          <Form.Item
            initialValue={excludeModelNames}
            name="excludeModelNames"
            label="排除模型名称"
          >
            <Select
              mode="tags"
              tokenSeparators={[',']}
              options={excludeModelNameOptions}
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default DataConfig;
