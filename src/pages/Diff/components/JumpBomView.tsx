import * as toBom from '@/api/toBom';
import { bomMiniappUrl } from '@/config';
import { pick } from 'lodash-es';
import { PropsWithChildren } from 'react';

function JumpBomView(props: PropsWithChildren<toBom.Params>) {
  async function handleJump() {
    const { uniqueId } = await toBom.setParams(
      pick(props, ['orderId', 'plankId', 'plank', 'highlight']),
    );
    const url = `${bomMiniappUrl}&miniappUploadId=${uniqueId}`;
    window.open(url);
  }

  return (
    <span onClick={handleJump}>
      {props.children || <button>跳转到BOM展示小程序</button>}
    </span>
  );
}

export default JumpBomView;
