import { createLog } from '@/utils/console';
import { useBoolean } from 'ahooks';
import { Button, Form, Modal, Select } from 'antd';
import { FC } from 'react';

const log = createLog('[page-comp: DiffConfig]');
const title = '差异配置';

interface IDiffConfig {
  omitKeys: string[];
}

interface IProps {
  diffConfig: IDiffConfig;
  onUpdateConfig: (config: IDiffConfig) => Promise<void>;
}

const DiffConfig: FC<IProps> = (props) => {
  const { diffConfig } = props;
  const { omitKeys } = diffConfig;
  const omitKeyOptions = diffConfig.omitKeys.map((key) => ({ value: key }));

  const [diffConfigForm] = Form.useForm<IDiffConfig>();
  const [
    diffConfigOpen,
    { setTrue: showDiffConfig, setFalse: hideDiffConfig },
  ] = useBoolean();

  async function handleOk() {
    const diffConfig = diffConfigForm.getFieldsValue();
    log({ diffConfig });
    await props.onUpdateConfig(diffConfig);
    hideDiffConfig();
  }
  function handleCancel() {
    diffConfigForm.resetFields();
    hideDiffConfig();
  }

  return (
    <>
      <Button type="primary" onClick={showDiffConfig}>
        {title}
      </Button>
      <Modal
        title={title}
        open={diffConfigOpen}
        onCancel={handleCancel}
        onOk={handleOk}
      >
        <Form form={diffConfigForm}>
          <Form.Item initialValue={omitKeys} label="排除的字段" name="omitKeys">
            <Select mode="tags" options={omitKeyOptions} />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default DiffConfig;
