import { PageContainer } from '@ant-design/pro-layout';
import { useModel } from '@umijs/max';
import { useMount } from 'ahooks';
import { DataDiffTable, EHiddenPart } from './components/DataDiff';

const log = console.log.bind(null, '[page: Diff/FormUpload]');

function IndexPage() {
  const {
    design: { oldPlanks: markedDatas, newPlanks: plankDatas },
  } = useModel('bom');
  useMount(() => {
    log('mount');
  });

  return (
    <PageContainer>
      <DataDiffTable
        oldPlanks={markedDatas}
        newPlanks={plankDatas}
        hiddenParts={[EHiddenPart.UpdateBase]}
      />
    </PageContainer>
  );
}

IndexPage.displayName = 'DiffFromUploadPage';

export default IndexPage;
