import { IPlank } from '@/types';
import { useSetState } from 'ahooks';

const useBom = () => {
  const [design, setDesign] = useSetState({
    designId: '',
    oldPlanks: [] as IPlank[],
    newPlanks: [] as IPlank[],
  });

  return {
    design,
    setDesign,
  };
};

export default useBom;
