import { useSetState } from 'ahooks';

const useConfig = () => {
  const [config, seConfig] = useSetState({});

  return {
    config,
    seConfig,
  };
};

export default useConfig;
