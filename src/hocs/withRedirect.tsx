import { Navigate } from '@umijs/max';
import React from 'react';

export const withRedirect = (
  Component: React.ComponentType,
  {
    useRedirect = () => {
      return '';
    },
  },
) => {
  const hoc = () => {
    const to = useRedirect();
    if (!to) {
      return <Component />;
    } else {
      return <Navigate to={to} />;
    }
  };
  hoc.displayName = `withRedirect<${Component.displayName}>`;
  return hoc;
};
