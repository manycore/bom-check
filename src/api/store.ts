import { APP_NAME } from '@/constants/app';

interface ISetItemParams {
  key: string;
  value: any;
}
interface IGetItemParams {
  key: string;
}
const namespace = `@${APP_NAME}/`;

export async function setItem(params: ISetItemParams): Promise<void> {
  const { key, value } = params;
  return await Manycore.Storage.Common.putItemAsync({
    key: genKey(key),
    value: { value },
  });
}
export async function getItem<T>(
  params: IGetItemParams,
): Promise<T | undefined> {
  const { key } = params;
  return (await Manycore.Storage.Common.getItemAsync(genKey(key)))?.value;
}

function genKey(key: string) {
  return namespace + key;
}
