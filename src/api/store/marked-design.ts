import * as store from '@/api/store';
import { uniq } from 'lodash-es';
import { compile } from 'path-to-regexp';

const toPath = compile('marked-design/user/:userId');
const getUser = async () => await Manycore.User.getUserAsync();

export async function addMarkedDesign(params: { designId: string }) {
  const { designId } = params;
  const { userId } = await getUser();
  const designs = await getMarkedDesigns();
  designs.push(designId);
  await store.setItem({ key: toPath({ userId }), value: uniq(designs) });
}
export async function getMarkedDesigns() {
  const { userId } = await getUser();
  const designs = await store.getItem<string[]>({ key: toPath({ userId }) });
  return designs || [];
}
