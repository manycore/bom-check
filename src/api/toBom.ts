import { bomMiniappId } from '@/config';
import { IPlank } from '@/types';

const log = console.log.bind(null, '[toBom]');

interface Highlight {
  holes: string[];
  grooves: string[];
}

export interface Params {
  orderId: string;
  plankId: string;
  plank: IPlank;
  highlight: Highlight;
}
export function setParams(params: Params) {
  log({ params });
  return Manycore.Miniapp.uploadDataAsync({
    miniappId: bomMiniappId,
    data: JSON.stringify(params),
  });
}
