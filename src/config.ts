const isDev = process.env.NODE_ENV === 'development';
let stage = 'prod';
const stage_env = stage !== 'prod' ? stage : '';
const env = isDev ? stage_env : '';
export const bomMiniappId = isDev ? 'bom' : '3FO4K4WC9HW4';
export const bomMiniappUrl = `https://${env}${
  env ? '.' : ''
}kujiale.com/pub/saas/workbench/miniapp?enableMiniappDev=true&launchMiniapp=${bomMiniappId}`;
