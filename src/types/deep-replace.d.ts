declare module 'deep-replace' {
  export function match(
    obj: object,
    valueTest: ((value: any) => boolean) | null,
    propertyTest: ((props: string) => boolean) | null,
    decoratorFn: ({
      val,
      obj,
      prop,
    }: {
      val: any;
      obj: object;
      prop: string;
    }) => void,
  ): void;
}
