import { IPlank } from '.';

interface IExtraInfo {
  updateTime: number;
}
export interface IPlankWithExtraInfo {
  plank: IPlank;
  extra?: Partial<IExtraInfo>;
}
