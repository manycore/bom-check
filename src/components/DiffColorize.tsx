import type { ThemeType } from 'json-diff';
import { colorizeToCallback } from 'json-diff';
import { DiffObject } from 'json-diff/lib/colorize';
import { ReactNode } from 'react';

interface Props {
  diff: DiffObject;
  className?: string;
}

export function DiffColorize(props: Props) {
  const { diff, className } = props;
  const nodes = colorizeToReactNodes(diff);
  return <pre className={`diff-colorize ${className}`}>{nodes}</pre>;
}

const theme: Record<ThemeType, (s: string) => ReactNode> = {
  ' ': (s: string) => <div className="bg-gray-100 text-gray-400">{s}</div>,
  '+': (s: string) => <div className="bg-red-100 text-red-400">{s}</div>,
  '-': (s: string) => <div className="bg-green-100 text-green-400">{s}</div>,
};

const colorizeToReactNodes = function (diff: DiffObject): ReactNode[] {
  const output: ReactNode[] = [];
  colorizeToCallback(diff, { maxElisions: 3 }, (color, line) => {
    output.push(theme[color](`${color}${line}`));
  });
  return output;
};
