import { Modal as AModal, ModalProps } from 'antd';
import { useState } from 'react';
import { FullScreenButton } from './Button';
import './index.less';

interface IProps extends ModalProps {
  fullscreen?: { default: boolean };
}

const fullscreenClass = 'modal-wrap-fullscreen';

function Modal(props: IProps) {
  const { fullscreen, ...aModalProps } = props;

  const [isFullScreen, setIsFullScreen] = useState(fullscreen?.default);

  return (
    <AModal
      {...aModalProps}
      title={
        <>
          <span style={{ display: 'inline-block' }}>{props.title}</span>
          <FullScreenButton
            onClick={() => {
              setIsFullScreen(!isFullScreen);
            }}
          />
        </>
      }
      className={
        isFullScreen
          ? `${props.className} ${fullscreenClass} ${fullscreenClass}--has-header`
          : props.className
      }
    />
  );
}
export default Modal;
