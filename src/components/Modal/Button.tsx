import { ArrowsAltOutlined } from '@ant-design/icons';
import { Modal } from 'antd';

export interface IProps {
  getModal?: () => ReturnType<typeof Modal.info>;
  onClick?: (isFullScreen: boolean) => void;
}
export function FullScreenButton(props: IProps) {
  const { getModal } = props;
  let isFullScreen = false;
  const getClassName = () => (isFullScreen ? 'modal-wrap-fullscreen' : '');
  const toggleFullScreen = () => {
    isFullScreen = !isFullScreen;
  };
  const onClick = () => {
    toggleFullScreen();
    getModal?.().update({ className: getClassName() });
    props.onClick?.(isFullScreen);
  };

  return (
    <>
      <button
        className="ant-modal-close"
        style={{ right: 46 }}
        type="button"
        onClick={onClick}
      >
        <span className="ant-modal-close-x">
          <ArrowsAltOutlined />
        </span>
      </button>
    </>
  );
}
