import { useEffect } from 'react';

/**
 *
 * @example
 * useLog({ var1, var2 }, { desc: 'debug', log: console.log });
 */
export function useLog(
  varRecord: Record<string, any>,
  options?: { desc?: string; log?: typeof console.log },
) {
  const { desc = '', log = console.log } = options || {};
  Object.entries(varRecord).map(([key, value]) => {
    useEffect(() => {
      log(desc, { [key]: value });
    }, [value]);
  });
}
