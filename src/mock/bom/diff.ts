import { cloneDeep } from 'lodash-es';
import { planks } from '.';

export function getDiffTestData() {
  const oldData = cloneDeep(planks);
  const newData = cloneDeep(oldData);
  // removed
  newData.pop();
  const plank_modified = newData.pop()!;
  plank_modified.holes = [{ a: 1 } as any];
  // Modified
  newData.push(plank_modified);
  // Added
  newData.push({
    modelId: 'added',
    a: 1,
    orderId: 'test_orderId',
    plankId: 'test_orderId',
    name: 'test_name',
    productName: 'test_productName',
  } as any);
  return { oldData, newData };
}
