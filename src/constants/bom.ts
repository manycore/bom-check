export const columns = [
  {
    title: '编号',
    dataIndex: 'code',
  },
  {
    title: '模型Id',
    dataIndex: 'modelId',
  },
  {
    title: '柜体名称',
    dataIndex: 'productName',
  },
  {
    title: '模型名称',
    dataIndex: 'name',
  },
  {
    title: 'plankId',
    dataIndex: 'plankId',
  },
  {
    title: '订单Id',
    dataIndex: 'orderId',
  },
  {
    title: '订单名称',
    dataIndex: 'orderName',
  },
];
