import axios from 'axios';
import FileSaver from 'file-saver';

export async function getBlob(url: string) {
  const res = await axios.get(url, {
    responseType: 'blob', // 切记类型 blob
  });
  let blob = new Blob([res.data]);
  return blob;
}

// 将file文件上传转化为base64进行显示
export function getBase64(file: File) {
  return new Promise<string>((resolve, reject) => {
    ///FileReader类就是专门用来读文件的
    const reader = new FileReader();
    //开始读文件
    //readAsDataURL: dataurl它的本质就是图片的二进制数据， 进行base64加密后形成的一个字符串，
    reader.readAsDataURL(file);
    // 成功和失败返回对应的信息，reader.result一个base64，可以直接使用
    reader.onload = () => resolve(<string>reader.result);
    // 失败返回失败的信息
    reader.onerror = (error) => reject(error);
  });
}

export function download(content: string, filename: string) {
  var blob = new Blob([content], { type: 'text/plain;charset=utf-8' });
  FileSaver.saveAs(blob, filename);
}
