import { EDiffType, getDiffList } from './diff';

describe('getDiffList', () => {
  it('array order', () => {
    const oldList = [{ id: 1, name: 'a', omitKey: '1', array: [1, 2, 3] }];
    const newList = [{ id: 1, name: 'a', omitKey: '2', array: [3, 2, 1] }];
    const expectedType = EDiffType.Unchanged;
    const [[actualType]] = getDiffList(oldList, newList, 'id', ['omitKey']);
    expect(actualType).toEqual(expectedType);
  });
  it('profile pass', () => {
    const profile = {
      curves: [{ type: 'line' }, { type: 'line' }, { type: 'line' }],
      points: [1, 2, 3, 4, 5, 6],
    };
    const oldList = [
      {
        id: 1,
        profile,
      },
    ];
    const newList = [
      {
        id: 1,
        profile: {
          ...profile,
          points: [6, 5, 4, 3, 2, 1],
        },
      },
    ];
    const expectedType = EDiffType.Unchanged;
    const [[actualType]] = getDiffList(oldList, newList, 'id', []);
    expect(actualType).toEqual(expectedType);
  });
  it('profile fail', () => {
    const profile = {
      curves: [{ type: 'line' }, { type: 'line' }, { type: 'line' }],
      points: [1, 2, 3, 4, 5, 6],
    };
    const oldList = [
      {
        id: 1,
        profile,
      },
    ];
    const newList = [
      {
        id: 1,
        profile: {
          ...profile,
          points: [6, 5, 4, 3, 2],
        },
      },
    ];
    const expectedType = EDiffType.Modified;
    // console.log(
    //   JSON.stringify(getDiffList(oldList, newList, 'id', []), null, 2),
    // );
    const [[actualType]] = getDiffList(oldList, newList, 'id', []);
    expect(actualType).toEqual(expectedType);
  });
});
