import { findIndex } from 'lodash-es';

export function updateList({
  list,
  item,
  key = 'id',
}: {
  list: any[];
  item: any;
  key?: string;
}) {
  const index = findIndex(list, { [key]: item[key] });
  if (index === -1) {
    list.push(item);
  } else {
    list.splice(index, 1, item);
  }
}
