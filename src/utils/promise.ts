export function createReqTip(
  name: string,
  handler: {
    success: (msg: string) => any;
    error: (msg: string) => any;
  },
) {
  return [
    () => {
      handler.success(`${name}成功`);
    },
    (error?: Error) => {
      handler.error(`${name}失败：${error?.message}`);
    },
  ] as const;
}

export const waitTime = (time: number | undefined) =>
  new Promise((res) => {
    setTimeout(res, time);
  });
