import type { ColumnType } from 'antd/es/table';
import { clone, find, isString } from 'lodash-es';

export function delColAction(columns: ColumnType<any>[], actionKey = 'action') {
  return columns.filter((column) => !(column?.key === actionKey));
}

export function cloneCol<T>(
  columns: ColumnType<T>[],
  options: { dataIndex: string },
) {
  const col = find(columns, { dataIndex: options.dataIndex });
  return clone(col);
}

export function transformCols<T>(
  columns: ColumnType<T>[],
  options: { dataIndexs: string[] },
) {
  return columns.filter((column) => {
    if (!isString(column.dataIndex)) return;
    return options.dataIndexs.includes(column.dataIndex);
  });
}

export function filterCol(
  columns: ColumnType<any>[],
  options: { dataIndexs: string[] },
) {
  return columns.filter((column) => {
    if (!isString(column.dataIndex)) return;
    return options.dataIndexs.includes(column.dataIndex);
  });
}

export function createTableItemIndexer<T>(list: T[]) {
  return list.indexOf.bind(list);
}
