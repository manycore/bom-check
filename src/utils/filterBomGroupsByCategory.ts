export function filterBomGroupsByCategory(
  groups: Manycore.Integration.Bom.BomGroupWithId[],
  categorys: string[],
) {
  // category=0>children
  return groups.map((group) => {
    const { children } = group;
    const filteredChildren = children?.filter(({ category }) => {
      return category && categorys.includes(category);
    });
    return {
      ...group,
      children: filteredChildren,
    };
  });
}
