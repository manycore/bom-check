import { deepRound } from './number';

const genObject = ({ number }: { number: string | number }) => ({
  string: 'string',
  nested: {
    again: {
      string: 'string',
      object: {},
      array: [],
      number,
    },
  },
});

describe('deepToRound', () => {
  test('precision=3 number', () => {
    expect(
      deepRound(
        genObject({
          number: -476.55000000000007,
        }),
        3,
      ),
    ).toMatchObject(
      genObject({
        number: -476.55,
      }),
    );
  });
  test('precision=3 number-string', () => {
    expect(
      deepRound(
        genObject({
          number: '-476.55000000000007',
        }),
        3,
      ),
    ).toMatchObject(
      genObject({
        number: '-476.55',
      }),
    );
  });
});
