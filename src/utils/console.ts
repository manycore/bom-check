import { bindKey } from 'lodash-es';

export function createLog(desc: string) {
  const log = bindKey(console, 'log', desc);
  return log;
}
