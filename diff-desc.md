# 符号说明

## 表格”差异“列：
- add：新增
- delete：减少
- json_diff：变化
- unchange：不变

## 数组：
- "+"：新增
- "-"：减少
- "~"：变化
- " "：不变

## 对象：
- "__old"：旧值
- "__new"：新值
- "__added"：新增
- "__deleted"：删除


# 例

## 数组：
```js
console.log(jsonDiff.diff([1,["a","b"],4], [1,["a","c"],4]));
// Output:
//  [ [ " ", 1 ], [ "~", [ [ " ", "a" ], [ "-", "b" ], [ "+", "c" ] ] ], [ " ", 4 ] ]
```

## 对象：
```js
console.log(jsonDiff.diff({ foo: 'bar', b:3}, { foo: 'baz', b:3}));
// Output:
// { foo: { __old: 'bar', __new: 'baz' } }

// ------

console.log(jsonDiff.diff({"a":[4,5]}, {"b":[4,6]}));
// Output:
//  { "a__deleted": [ 4, 5 ], "b__added": [ 4, 6 ] }
```


# 参考
- [json-diff](https://www.npmjs.com/package/json-diff/v/0.9.0)